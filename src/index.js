import './stylesheets/main.scss';

/**
 * Toasties!
 *
 * @version 0.1
 * @author Kieran Parker <kieranmarker@gmail.com>
 * @copyright 2017
 * @licence MIT
 */
export class Toastie {

    /**
     * Set the position to left, center (default) or right - It will always be centered on
     * mobile.
     *
     * @constructor
     * @param  {('left', 'center', 'right')} [position=center] - The allowed display positions.
     * @param  {boolean} [margin=false] - If the message should have a small amount of whitespace surrounding it.
     * @param  {number} [timeout_length=6000] - The time
     * @throws {Error} Will throw an error if the provided parameters are incorrect.
     */
    constructor (position = 'center', margin = false, timeout_length = 6000) {
        if (['left', 'center', 'right'].includes(position) === false) {
            throw new Error(`"${position}" is not a valid position`);
        }

        if (typeof margin !== 'boolean') {
            throw new Error(`Margin "${margin}" is not a boolean.`);
        }

        if (Number.isInteger(timeout_length) === false) {
            throw new Error(`timeout length "${timeout_length}" is not a number.`);
        }

        this.position       = position;
        this.margin         = margin;
        this.timeout_length = timeout_length;
        this.queue          = [];
    }

    /**
     * Open a toast message and automatically hides it after 6 seconds.
     * An action button will be displayed if the action_text
     * and callback parameters are passed together.
     *
     * @param  {string} text - The text to be displayed in the toast message.
     * @return {boolean} - Will return true if the action is selected, otherwise false will be returned.
     * @throws {Error} No text was passed to Toastie!
     */
    newMessage (text) {
        if (!text) throw 'No text passed to Toastie!';

        let toast_element = document.createElement('div');

        toast_element.classList.add('toastie');
        toast_element.classList.add((this.margin) ? 'toastie-margin' : 'toastie-no-margin');
        toast_element.classList.add(`toastie-${this.position}`);
        toast_element.innerHTML = `<span class="toastie-text">${text}</span>`;

        this._queueSystem(toast_element);
    }

    /**
     * A simple queue system for pushing and popping messages.
     *
     * @private
     * @param {Element} message - Element to be rendered.
     */
    _queueSystem (message) {
        this.queue.push(message);

        if (this.queue.length < 2) {
            return this._displayMessage(message);
        }
    }

    /**
     * Rendering the removing the message.
     *
     * @private
     * @param {Node} message - Message to be displayed within the toast box.
     */
    _displayMessage (message) {
        document.body.appendChild(message);

        setTimeout(() => {
            message.classList.add('toastie-leaving');

            setTimeout(() => {
                message.parentNode.removeChild(message);

                this.queue.shift();

                if (this.queue.length > 0) {
                    this._displayMessage(this.queue[0]);
                }
            }, 750);
        }, this.timeout_length);
    };
}
