const webpack        = require('webpack');
const path           = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

const config = {
    entry: ['babel-polyfill', './src/index.js'],
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: './toasties.bundle.js'
    },
    resolve: {
        modules: [path.resolve(__dirname, 'node_modules')],
        extensions: ['', '.js', '.json'],
    },
    module: {
        loaders: [
            {   // Babel
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015']
                }
            },
            {   // SCSS
                test: /\.scss$/,
                loaders: ['style-loader', 'css-loader', 'sass-loader']
            },
        ]
    },
    plugins: [
        new UglifyJSPlugin()
    ]
};

module.exports = config;
