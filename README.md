# Toasties

A small library for displaying toast messages following the design patterns of [Material Design](https://material.io/guidelines/components/snackbars-toasts.html#snackbars-toasts-specs).

## Browser Support

- IE 11+
- Edge 12+
- Chrome 49+
- FireFox 44+
- Safari 10+

## Requirements

- Node.js v6

## Installation

```bash
$ npm install
```

## How to

Three parameters can be passed to Toastie on initiation:

1. ['left', 'center', 'right'] - The position on the screen the toast message will be displayed. Defaults to 'center'.
2. [false, true] - If the toast message should have a small margin surrounding it. Defaults to false (off).
3. How many milliseconds the message will be displayed for. Defaults to 6000.

First, create a new instance of Toastie:

```javascriptl
const Toast = new Toastie('center', false, 2000);
```

And then pass a message:

```javascript
Toast.newMessage('Hey, look at me!');
```
